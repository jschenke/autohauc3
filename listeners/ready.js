const { Listener } = require('discord-akairo');
const { logInfo, logWarn, logError } = require('../bot');
const { serverID, memberCountVCID } = require('../config.json');

class ReadyListener extends Listener {
    constructor() {
        super('ready', {
            emitter: 'client',
            event: 'ready'
        });
    }

    async exec(...args) {
        const client = this.client;

        setInterval(() => {
            try {
                let guild = client.guilds.resolve(serverID);
                guild.members.fetch().then(async fetchedMembers => {
                    let memberCount = fetchedMembers.filter(member => !member.user.bot).size;
                    let channel = client.channels.resolve(memberCountVCID);
                    if (channel.isVoice()) channel.setName('\uD83D\uDC65 Members: ' + memberCount);
                    else logError('Member count channel ID is not a voice channel').catch(() => console.error('Member count channel ID is not a voice channel'));
                })
            } catch(e) { logError(e).catch(() => console.error(e)); }
        }, 2*60*1000);

        await logInfo('I\'m ready!');
    }
}

module.exports = ReadyListener;