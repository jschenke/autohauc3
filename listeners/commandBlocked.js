const { Listener } = require('discord-akairo')
const { logInfo, logError} = require('../bot')

class CommandBlockedListener extends Listener {
    constructor() {
        super('commandBlocked', {
            emitter: 'commandHandler',
            event: 'commandBlocked'
        });
    }

    async exec(message, command, reason) {
        await logInfo(`${message.author.username} was blocked from using ${command.id} because of ${reason}!`).catch(e => logError(e).catch(() => console.error(e)))
    }
}

module.exports = CommandBlockedListener
