const { Listener } = require('discord-akairo');
const { logInfo, logError } = require('../bot');
const { blacklistedUsers } = require('../config.json');

class NewMemberListener extends Listener {
    constructor() {
        super('newmember', {
            emitter: 'client',
            event: 'guildMemberAdd'
        });
    }

    async exec(member) {
        logInfo('New Member: ' + member.tag).catch( e => logError(e).catch(() => console.error(e)));
        try {
            if (blacklistedUsers.includes(member.id)) {
                await logInfo('Blacklisted Member Joined: ' + member.tag);
                return member.ban({ reason: 'Blacklist' });
            }
        } catch(e) { logError(e).catch(() => console.error(e)); }
    }
}

module.exports = NewMemberListener;