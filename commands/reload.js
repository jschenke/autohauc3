const { Command } = require('discord-akairo');

class ReloadCommand extends Command {
    constructor() {
        super('reload', {
            aliases: ['reload'],
            args: [
                {
                    id: 'commandID'
                }
            ],
            description: {
                usage: 'reload <command>',
                examples: ['reload reload'],
                description: 'Reload a command'
            },
            ownerOnly: true,
            category: 'owner'
        });
    }

    exec(message, { commandID }) {
        // `this` refers to the command object
        this.handler.reload(commandID);
        return message.reply(`Reloaded command ${commandID}`);
    }
}

module.exports = ReloadCommand;