const { Command } = require('discord-akairo');

class ReloadAllCommand extends Command {
    constructor() {
        super('reloadall', {
            aliases: ['reloadall'],
            ownerOnly: true,
            category: 'owner',
            description: {
                usage: 'reloadall',
                examples: ['reloadall'],
                description: 'Reloads all commands'
            }
        });
    }

    exec(message, args) {
        // `this` refers to the command object
        this.handler.reloadAll();
        return message.reply(`Reloaded all commands`);
    }
}

module.exports = ReloadAllCommand;
