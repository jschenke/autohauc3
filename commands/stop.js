const { Command } = require('discord-akairo')
const { logError } = require("../bot");

class StopCommand extends Command {
    constructor() {
        super('stop', {
            aliases: ['stop', 'shutdown'],
            ownerOnly: true,
            category: 'owner',
            description: {
                usage: 'stop',
                examples: ['stop'],
                description: 'Stop the bot'
            }
        });
    }

    async exec(message, args) {
        message.channel.send(':octagonal_sign: Stopping...').then(() => {
            this.client.destroy();
            process.exit(0);
        }).catch(e => logError(e).catch(() => console.error(e)));
    }
}

module.exports = StopCommand