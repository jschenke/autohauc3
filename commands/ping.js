const { Command } = require('discord-akairo');

class PingCommand extends Command {
    constructor() {
        super('ping', {
            aliases: ['ping'],
            category: 'other',
            description: {
                usage: 'ping',
                examples: ['ping'],
                description: 'Pong!'
            }
        });
    }

    exec(message, args) {
        return message.reply('Pong!');
    }
}

module.exports = PingCommand;