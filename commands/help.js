const { MessageEmbed } = require('discord.js');
const { Command } = require('discord-akairo');

function formatName(str) {
    return str.slice(0, 1).toUpperCase() + str.slice(1).toLowerCase();
}

class HelpCommand extends Command {
    constructor() {
        super('help', {
            aliases: ['help', 'h', 'halp', 'commands'],
            category: 'other',
            args: [
                {
                    id: 'command',
                    type: 'commandAlias',
                    match: 'content',
                    default: null
                }
            ],
            description: {
                usage: 'help [command]',
                examples: ['help', 'commands', 'h'],
                description: 'Displays the commands of the bot'
            }
        });
    }

    async exec(message, { command }) {
        if (!command) {
            const embed = new MessageEmbed();
            this.handler.categories.forEach((cm, category) => {
                const dirSize = cm.filter(cmd => cmd.category === cm);
                let mappedOut = cm.map(x => `\`${x}\``).join(', ');

                if (category === 'owner' && !this.client.ownerID.includes(message.author.id)) mappedOut = '`No commands available..`';
                embed.addField(`${dirSize.size} | **${formatName(category)} Commands**`, mappedOut)
                    .setColor('#88eaff')
                    .setAuthor(`Help Menu | ${message.guild.name}`, message.guild.iconURL());
            });

            return message.channel.send({ embed });
        } else if (command) {
            const cmd = command;
            const embed = new MessageEmbed()
                .setColor('#88eaff')
                .setAuthor(`Help: ${formatName(cmd.aliases[0])} | ${message.guild.name}`, message.guild.iconURL())
                .setDescription(`
                **Command Name**: \`${cmd.aliases[0]}\`
                **Command Aliases**: ${`${cmd.aliases.map(x => `\`${x}\``).join(', ') || 'No Alias'}`}
                **Owner Only**: \`${cmd.ownerOnly ? 'Yes' : 'No' || 'No'}\`
                
                **Command Description**: ${cmd.description.description || 'A command'}
                **Command Usage**: \`${cmd.description.usage || cmd.aliases[0]}\`
                **Command Examples**:\n\`\`\`${cmd.description.examples.join('\n') || cmd.aliases[0]}\`\`\``)
                .setFooter(`Syntax: <required> : [optional]`);
            return message.channel.send({ embed });
        }
    }
}

module.exports = HelpCommand;
