const { Command } = require('discord-akairo');

class DeopCommand extends Command {
    constructor() {
        super('deop', {
            aliases: ['deop'],
            category: 'other',
            description: {
                usage: 'deop',
                examples: ['deop'],
                description: 'Removes operator role from a user'
            }
        });
    }

    exec(message, args) {
        return message.reply('Soon\u2122');
    }
}

module.exports = DeopCommand;