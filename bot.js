const { AkairoClient, CommandHandler, InhibitorHandler, ListenerHandler } = require('discord-akairo');
const { Intents } = require('discord.js');
const { token, owners } = require('./config.json');
const chalk = require('chalk');

class MyClient extends AkairoClient {
    constructor() {
        super({
            ownerID: owners
        }, {
            disableMentions: 'everyone',
            presence: {
                status: 'online',
                afk: false,
                activity: {
                    name: 'the rain',
                    type: 'WATCHING',
                    url: 'https://twitch.tv/thehauc3'
                }
            },
            intents: [ Intents.FLAGS.GUILDS, Intents.FLAGS.DIRECT_MESSAGES ]
        });

        this.commandHandler = new CommandHandler(this, {
            directory: './commands/',
            prefix: '.'
        });

        this.inhibitorHandler = new InhibitorHandler(this, {
            directory: './inhibitors/'
        });

        this.listenerHandler = new ListenerHandler(this, {
            directory: './listeners/'
        });

        // this.listenerHandler.setEmitters({
        //     commandHandler: this.commandHandler,
        //     inhibitorHandler: this.inhibitorHandler,
        //     listenerHandler: this.listenerHandler
        // })
    }

    async login(token) {
        this.commandHandler.loadAll();
        this.commandHandler.useInhibitorHandler(this.inhibitorHandler);
        this.inhibitorHandler.loadAll();
        this.commandHandler.useListenerHandler(this.listenerHandler);
        this.listenerHandler.loadAll();

        return super.login(token);
    }
}

const logInfo = async msg => {
    console.log(chalk.blue.bold('[INFO] ') + chalk.reset(msg));
}
module.exports.logInfo = logInfo;

const logWarn = async msg => {
    console.log(chalk.yellow.bold('[WARN] ') + chalk.reset(msg));
}
module.exports.logWarn = logWarn;

const logError = async msg => {
    console.log(chalk.red.bold('[ERROR] ') + chalk.reset.red(msg));
}
module.exports.logError = logError;

const client = new MyClient()
// client.login(token).catch(e => logError(e).catch(() => console.error(e)))
client.login(token).catch(console.error)